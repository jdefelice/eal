/**
 * Imports
 */
import Vue from 'vue';

/*
Example vue.js component, not used in this test.
 */
// import Example from './components/example.vue';
// Vue.component('example', Example);

/**
 * Vue JS instance for the entire app.
 * @type {CombinedVueInstance<V extends Vue, Object, Object, Object, Record<never, any>>}
 */
var app = new Vue({
    el: '#app',
    data() {
        return {
            menuOpen: false,
            searchOpen: false
        }
    },
    methods: {
        toggleMenu() {
            this.menuOpen = !this.menuOpen;
        },
        toggleSearch() {
            this.searchOpen = !this.searchOpen;
        }
    }
});

/**
 * Toggle the expanding of mobile navigation menu items.
 */
var menuItemToggles = document.querySelectorAll(".nav__drop_toggle");
for (var i = 0; i < menuItemToggles.length; i++) {
    menuItemToggles[i].addEventListener("click", (e) => {
        e.target.classList.toggle('nav__drop_toggle--open');
    });
}
