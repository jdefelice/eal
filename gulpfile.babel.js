'use strict';

import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import stylelint from 'gulp-stylelint';
import imagemin from 'gulp-imagemin';
import babel from 'gulp-babel';
import sourcemaps from 'gulp-sourcemaps';
import vueify from 'gulp-vueify';
import browserify from 'gulp-browserify';
import livereload from 'gulp-livereload';

function swallowError(error) {

    // If you want details of the error in the console
    console.log(error.toString())

    this.emit('end')
}

gulp.task('css', () => {
    gulp.src('src/scss/style.scss')
        .pipe(stylelint({
            reporters: [
                {formatter: 'string', console: true}
            ]
        }))
        .pipe(sass({outputStyle: 'compressed'}))
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .on('error', swallowError)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/css'))
        .pipe(livereload());
});

gulp.task('images', () => {
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'));
});

gulp.task('js', () => {
    gulp.src('src/js/app.js')
        .pipe(babel())
        .pipe(browserify( { transform: 'vueify' }))
        .pipe(gulp.dest('dist/js'))
        .pipe(livereload());
});

gulp.task('watch', () => {
    livereload.listen();
    gulp.watch('src/scss/**/*.scss', ['css']);
    gulp.watch('src/images/*', ['images']);
    gulp.watch('src/js/**/*.js', ['js']);
});

gulp.task('default', ['css', 'js', 'images']);

